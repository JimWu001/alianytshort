import {StyleSheet, View, ActivityIndicator} from 'react-native';
import React, { useState } from 'react';
import axios from 'axios';
import Shorts from './src/Shorts';

// "THE FIRST TAKE"的 Shorts 列表URL
const baseURL = "https://yt.lemnoslife.com/channels?part=shorts&id=UC9zY_E8mcAo_Oq772LEZq8Q";

type Item = {
  id: string;
  url: string;
};

const App = () => {
  const [playItems, setPlayItems] = useState([]);
  const [loading, setLoading] = useState(true);

  React.useEffect(() => {
    axios.get(baseURL).then((response) => {
      let updateItems = [];
      // 重組取回來的short list data
      for (let i=0; i<response.data.items[0].shorts.length; i++) {
        updateItems[i] = {id: i, url:'https://youtube.com/shorts/'+response.data.items[0].shorts[i].videoId};
      }

      setPlayItems(updateItems);
      setLoading(false);
    });
  }, []);  

  return (
    <View style={styles.container}>
      { loading ? <ActivityIndicator size="large"/> : <Shorts items={playItems} /> }
    </View>
  );
};

// 樣式
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    backgroundColor:'black',
  },
});

export default App;
