#需求1
- 側邊的按鈕不需要有功能，只要樣式一樣就好
- 影片下方的影片名稱跟 Hash Tag 不用做，只要乾淨的影片就好。
- 需要可以點擊暫停影片。
- 需要可以靜音。
- 可以播放並且操控影片就好，不用糾結在隱藏 YouTube 的一些遮罩（若有辦法處理掉也可）。

#需求2
- 取得『THE FIRST TAKE』的 Shorts 列表

#需求3:
- 可以參照 YouTube Shorts 網頁版，使用『滾輪滑動』或手機版的『拖曳滑動』，二擇一製作即可。
- 滑動到下一個影片時，要『暫停』上一個影片並且播放當前的影片。
- 返回上一個影片時，要從之前暫停的地方播放。

#注意事項:
- 請依照應徵的職位選擇 React, React Native 或 Vue 製作，不侷限框架。
- 不限使用任何狀態管理。
- 不限任何 css 框架。
- 請使用 Git。
- 請注意網站的渲染效能。
- 請注意網站的渲染效能。
- 請注意網站的渲染效能。

===========================================================
#使用教學
- 先建構環境,請參考官網 https://reactnative.dev/docs/environment-setup?platform=android
- 下載好source code
- 到專案根目錄下執行 "yarn install" 安裝所需套件
- 在專案根目錄下執行 "yarn start" 即可選擇要執行的模擬器


### ps: android手機可以直接安裝打包好的apk位置如下: 
https://drive.google.com/file/d/1cHtsWp_wezIhS2-I1ElspWCGcQLwXOr5/view?usp=sharing



Author By JimWu