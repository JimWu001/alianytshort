import React, {useRef} from 'react';
import { TouchableOpacity, LayoutRectangle, StyleSheet, View, Text } from 'react-native';
import YoutubePlayer, { YoutubeIframeRef } from 'react-native-youtube-iframe';

export const getYoutubeIdFromURL = (url: string): string | undefined => {
  if (url.includes('?')) {
    const arr = url.split('?');
    arr.pop();

    url = arr[0];
  }

  const arr = url.split(/(vi\/|v%3D|v=|\/v\/|youtu\.be\/|\/embed\/)/);
  const youtubeId = undefined !== arr[2] ? arr[2].split(/[^\w-]/i)[0] : arr[0];

  if (youtubeId.includes('https://youtube.com/shorts/')) {
    return youtubeId.replace('https://youtube.com/shorts/', '');
  }
  
  return youtubeId;
};

type ShortItemProps = {
  index: number;
  visible: boolean;
  playing: boolean;
  paused: boolean;
  url: string;
  layout: LayoutRectangle;
};

function ShortItem({visible, playing, url, layout}: ShortItemProps) {
  const youtubeId = getYoutubeIdFromURL(url);
  const youtubePlayerRef = useRef<YoutubeIframeRef>(null);

  return (
    <View style={styles.container}>
        <YoutubePlayer
          ref={youtubePlayerRef}
          height={layout.height}
          width={layout.width-70}
          videoId={youtubeId}
          play={playing}
          onChangeState={(event: string) => {
            if (event === 'ended' && visible) {
              youtubePlayerRef?.current?.seekTo(0, true);
            }
          }}
          webViewProps={{
            injectedJavaScript: `
              var element = document.getElementsByClassName('container')[0];
              element.style.position = 'unset';
              true;
            `,
          }}
        />

      {/* Buttons */}
      <View style={styles.buttonContainer}>
          <TouchableOpacity 
            style={styles.button}
            onPress={console.log("press Like")}>
              <Text style={styles.baseText}>Like</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.button}
            onPress={console.log("press Bad")}>
              <Text style={styles.baseText}>Bad</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.button}
            onPress={console.log("press info")}>
              <Text style={styles.baseText}>info</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.button}
            onPress={console.log("press Share")}>
              <Text style={styles.baseText}>Share</Text>
          </TouchableOpacity> 
          <TouchableOpacity 
            style={styles.button}
            onPress={console.log("press ...")}>
              <Text style={styles.baseText}> . . . </Text>
          </TouchableOpacity>                                
      </View>

    </View>
  );
}

// 樣式
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
  },
  buttonContainer: {
    backgroundColor:'black',
    marginLeft: 15,
    marginTop:200,
  },
  button:{    
    marginTop: 30,
    height: 55,
    width: 55,
    borderRadius:400, 
    backgroundColor:'grey',
    justifyContent: 'center',
    alignItems: 'center',
  },
  baseText: {
    fontWeight: 'bold',
    fontSize: 13,
  },    
});

export default ShortItem;
